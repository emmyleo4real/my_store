Important Notes:
•	This application is about displaying a product, add it to cart and calculate the total and tax then display it.
•   This is a web application for a self-order kiosk.
•	5 products is available for purchase.  
•	When the user clicks on the button to add a product to the order, it ask the user for the product quantity in a pop up.
•	When the user clicks on the checkout button, it ask the customer for their name in a pop up to place the order. 
•	The following user input must be validated:
•	Product quantity should be numeric only. 
•	The user must enter their name to checkout. 
•	On successful checkout, generate a receipt to give to the customer, including all the products bought, quantity, cost, taxes, total cost and customer name. The receipt only show the products the user bought. 
•	It Calculate and include GST (@ 13%) in the receipt.