// DEFINE OBJECT AS CLASSES
class Book {
  constructor(id, name, author, cost, image) {
    this.id = id
    this.name = name
    this.author = author
    this.cost = cost
    this.quantity = 0
    this.image = image
  }
}

class Shelf {
  constructor() {
    this.books = []
    this.length = 0
  }

  getBook(id) {
    return this.books.find((b) => b.id == id)
  }

  getBooks() {
    return this.books
  }

  addBookToShelf(book) {
    // create and increase count
    this.books.push(book)
    this.length++
    return this
  }
}

class Cart {
  constructor() {
    this.books = []
    this.length = 0
  }

  getCartBooks() {
    return this.books
  }

  addBookToCart(book, qty) {
    if (!qty || typeof qty != "number" || isNaN(qty)) {
      return
    }

    // hold if the book already exists
    let existBook = false

    this.books = this.books.map((b) => {
      if (b.id === book.id) {
        existBook = true
        b.quantity += qty
      }
      return b
    })

    // if book does not exists, add the book
    if (!existBook) {
      book.quantity = qty
      this.books.push(book)
      this.length++
    }
    return this
  }
}

// declare variables
let bookId = ""

// create and empty cart
const cart = new Cart()
const Receipt = []

// create some Books
const book1 = new Book("B123", "English Grammar in Use", "Raymond Murphy", 2500, "english")
const book2 = new Book("B124", "Anatomy and Physiology", "Neal Cook, et al.", 5200, "medical")
const book3 = new Book("B125", "Harry Potter and Philosopher's Stone", "J.K. Rowling", 1300, "novel")
const book4 = new Book("B126", "HTML + CSS Web Design", "Jon Duckett", 3100, "webdesign")
const book5 = new Book("B127", "A Nazareth Manifesto", "Wiley Blackwell", 2600, "faith")
const book6 = new Book("B128", "Social Engineering", "Christopher Hadnagy", 3810, "engineering")
const book7 = new Book("B129", "Concise Oxford Dictionary", "Oxford", 2500, "dictionary")
const book8 = new Book("B120", "Javascript & Jquery", "Jon Duckett", 2130, "javascript")

// add Books to shelf
const bookShelf = new Shelf()
bookShelf.addBookToShelf(book1)
bookShelf.addBookToShelf(book2)
bookShelf.addBookToShelf(book3)
bookShelf.addBookToShelf(book4)
bookShelf.addBookToShelf(book5)
bookShelf.addBookToShelf(book6)
bookShelf.addBookToShelf(book7)
bookShelf.addBookToShelf(book8)

// function for printing receipt
function printElement() {
  const elem = document.querySelector("#printThis")
  var domClone = elem.cloneNode(true)

  // hide the modal
  document.querySelector("#myModal").style.display = "none"

  var $printSection = document.querySelector("#printSection")

  if (!$printSection) {
    var $printSection = document.createElement("div")
    $printSection.id = "printSection"
    document.body.appendChild($printSection)
  }

  $printSection.innerHTML = ""
  $printSection.appendChild(domClone)
  window.print()
}

// function for calculating GST
function calcGST(n) {
  return Number(((13 / 100) * n).toFixed(2))
}

// show books in UI
const productsUl = document.querySelector("#productsUl")

const shelfBooks = bookShelf.getBooks()
shelfBooks.forEach((b) => {
  const productLi = `
<li>
  <img src="./img/${b.image}.jpeg" alt="${b.name}" />
  <br />
  <span>${b.name}</span><br />
  <span>Author: ${b.author}</span><br />
  <span>$${b.cost}</span><br />
  
  <button class='addBtn' data-id='${b.id}'>
  Add to Cart</button>
</li>
`
  productsUl.insertAdjacentHTML("beforeend", productLi)
})

// hide the buttons by default
document.querySelector("#checkOut").style.visibility = "hidden"

// show check out pop
document.querySelector("#checkOut").addEventListener("click", (e) => {
  document.querySelector("#addForm").style.display = "none"
  document.querySelector("#checkForm").style.display = "block"
  document.querySelector("#makeReceipt").disabled = true
})

// close check out popup
document.querySelector("#checkClose").addEventListener("click", (e) => {
  document.querySelector("#checkForm").style.display = "none"
})

// show add to cart popup
document.querySelectorAll(".addBtn").forEach((btn) => {
  btn.addEventListener("click", (e) => {
    // close the check out pop
    document.querySelector("#checkForm").style.display = "none"

    // get the book with id
    const book = bookShelf.getBook(btn.dataset.id)
    bookId = book.id
    document.querySelector("#bookImg").src = `./img/${book.image}.jpeg`
    document.querySelector("#bookName").innerHTML = book.name
    document.querySelector("#bookAuthor").innerHTML = book.author
    document.querySelector("#bookCost").innerHTML = book.cost
    const addCartBtn = document.querySelector("#addToCart")
    addCartBtn.disabled = true
    addCartBtn.classList.add("disabled")

    document.querySelector("#addForm").style.display = "block"
  })
})

// close add to cart popup
document.querySelector("#addClose").addEventListener("click", (e) => {
  document.querySelector("#addForm").style.display = "none"
})

// make sure the quantity input is valid
document.querySelector("#bookQty").addEventListener("input", (e) => {
  const val = Number(e.target.value)
  const addCartBtn = document.querySelector("#addToCart")
  if (val && !isNaN(val) && val >= 1) {
    addCartBtn.disabled = false
    addCartBtn.classList.remove("disabled")
  } else {
    addCartBtn.disabled = true
    addCartBtn.classList.add("disabled")
  }
  e.target.value = parseInt(val)
})

// add item to cart
document.querySelector("#addToCart").addEventListener("click", (e) => {
  const bookQty = Number(document.querySelector("#bookQty").value)

  if (!bookQty || isNaN(bookQty)) {
    // alert the customer
    document.querySelector("#qtyWarn").innerHTML = "Quantiity must be a number greater than 1"
    return
  }

  // get the book
  const book = bookShelf.getBook(bookId)

  // add the book to the cart
  cart.addBookToCart(book, bookQty)

  // clear the input
  document.querySelector("#bookQty").value = null
  e.target.disabled = true
  e.target.classList.add("disabled")
  document.querySelector("#checkOut").style.visibility = "visible"
})

// make sure the name input is valid
document.querySelector("#uname").addEventListener("input", (e) => {
  const val = e.target.value.trim()
  const makeReceipt = document.querySelector("#makeReceipt")
  if (val && val != "") {
    makeReceipt.disabled = false
    makeReceipt.classList.remove("disabled")
  } else {
    makeReceipt.disabled = true
    makeReceipt.classList.add("disabled")
  }
})

// close Modal
document.querySelectorAll(".closeModal").forEach((btn) => {
  btn.addEventListener("click", (e) => {
    document.querySelector("#myModal").style.display = "none"
  })
})

// add use items in cart to make receipt
document.querySelector("#makeReceipt").addEventListener("click", (e) => {
  // calculate the recipt using the cart and GST()
  // get all the books in the cart
  const books = cart.getCartBooks()

  // set the customer name
  document.querySelector("#custName").innerHTML = document.querySelector("#uname").value

  const receiptEl = document.querySelector("#receiptData")
  receiptEl.innerHTML = ""
  let totalNet = 0

  books.forEach((b, i) => {
    b.totalCost = Number((b.cost * b.quantity).toFixed(2))
    totalNet += b.totalCost

    const dataTr = `
    <tr>
      <td>${i + 1}</td>
      <td>${b.name}</td>
      <td>${b.quantity}</td>
      <td>$${b.cost.toLocaleString("en-US")}</td>
      <td>$${b.totalCost.toLocaleString("en-US")}</td>
    </tr>
    `
    receiptEl.insertAdjacentHTML("beforeend", dataTr)
  })

  const gstValue = calcGST(totalNet)
  totalNet += gstValue

  const gstTr = `
  <tr>
    <td style="text-align: right;" colspan="4">GST @ 13% </td>
    <td colspan="1">$${gstValue.toLocaleString("en-US")}</td>
  </tr>
  `
  receiptEl.insertAdjacentHTML("beforeend", gstTr)

  const totalTr = `
  <tr>
    <td colspan="3">Total</td>
    <td colspan="2">$${totalNet.toLocaleString("en-US")}</td>
  </tr>
  `
  receiptEl.insertAdjacentHTML("beforeend", totalTr)

  console.log(books)
  // return

  // hide the check out form
  document.querySelector("#checkForm").style.display = "none"
  document.querySelector("#uname").value = null

  // show the modal
  document.querySelector("#myModal").style.display = "block"
})

// print the recipt
document.querySelector("#btnPrint").addEventListener("click", (e) => {
  printElement()
})
